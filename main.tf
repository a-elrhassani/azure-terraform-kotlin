provider "azurerm" {
  features {}

  subscription_id   = "dec0ce65-10e5-4aba-b2f8-f3e13eed8429"
  tenant_id         = "77aa1369-be4d-4759-b356-69f89d50151c"
  client_id         = "048ab957-2d92-45f6-893c-f6ee7691d6d4"
  client_secret     = "ud58Q~Vb1lnvK8HMb9ciHRWuX4vwjCFqKX3K~aLk"
}

resource "azurerm_resource_group" "terraform_demo" {
  location  = "germanywestcentral"
  name      = "tf_main"
}

resource "azurerm_container_group" "terraform_container_group" {
  location            = azurerm_resource_group.terraform_demo.location
  name                = "demoapp"
  os_type             = "Linux"
  ip_address_type     = "Public"
  dns_name_label      = "demoappdns"
  resource_group_name = azurerm_resource_group.terraform_demo.name
  container {
    cpu         = 1
    image       = "aelrhassani/demoapp"
    memory      = 1
    name        = "demoapp"
    ports {
      port = 80
    }
  }
}
