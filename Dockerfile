FROM gradle:4.7.0-jdk8-alpine AS build
COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN ./gradlew build --no-daemon


FROM openjdk:8-jdk-alpine
WORKDIR /app
EXPOSE 80
ONBUILD COPY --from=build /home/gradle/src/build/libs/*.jar /app/application.jar
ENTRYPOINT ["java","-jar","/demo.jar"]
