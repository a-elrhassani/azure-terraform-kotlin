package com.example.demo.services

import com.example.demo.models.User

interface UserService {
    fun getUserById(userId: Long): User
    fun getAllUsers(): List<User>
    fun createUser(user: User): User
    fun updateUser(userId: Long, user: User): User
    fun deleteUserById(userId: Long)
    fun findUserByEmail(email: String): User?
}
