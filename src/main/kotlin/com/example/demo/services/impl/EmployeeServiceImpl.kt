package com.example.demo.services.impl

import com.example.demo.models.Employee
import com.example.demo.exceptions.RecordNotFoundException
import com.example.demo.exceptions.HandleIllegalStateException
import com.example.demo.repositories.EmployeeRepository
import com.example.demo.services.EmployeeService
import org.springframework.stereotype.Service
import kotlin.reflect.KClass

@Service
class EmployeeServiceImpl(private val employeeRepository: EmployeeRepository) :
    EmployeeService {

    override fun getEmployeesById(employeeId: Long): Employee = employeeRepository.findById(employeeId)
        .orElseThrow { RecordNotFoundException("No matching employee was found") }

    override fun getAllEmployees(): List<Employee> = employeeRepository.findAll()

    override fun createEmployee(employee: Employee): Employee = employeeRepository.save(employee)

    override fun updateEmployeeById(employeeId: Long, employee: Employee): Employee {
        if (employeeId < 1) throw HandleIllegalStateException("The Id provided is not valid")
        return if (employeeRepository.existsById(employeeId)) {
            employeeRepository.save(employee)
        } else throw RecordNotFoundException("No matching employee was found")
    }

    override fun deleteEmployeesById(employeeId: Long) {
        return if (employeeRepository.existsById(employeeId)) {
            employeeRepository.deleteById(employeeId)
        } else throw RecordNotFoundException("No matching employee was found")
    }

}

