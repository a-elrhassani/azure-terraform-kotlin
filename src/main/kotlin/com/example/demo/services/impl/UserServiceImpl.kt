package com.example.demo.services.impl

import com.example.demo.exceptions.HandleIllegalStateException
import com.example.demo.exceptions.RecordNotFoundException
import com.example.demo.models.User
import com.example.demo.repositories.UserRepository
import com.example.demo.services.UserService
import org.springframework.stereotype.Service

@Service
class UserServiceImpl(private val userRepository: UserRepository) : UserService {
    override fun getUserById(userId: Long): User =
        userRepository.findById(userId).orElseThrow { RecordNotFoundException("No matching user was found") }

    override fun findUserByEmail(email: String): User? = this.userRepository.findUserByEmail(email)

    override fun getAllUsers(): List<User> = userRepository.findAll()

    override fun createUser(user: User): User = userRepository.save(user)

    override fun updateUser(id: Long, user: User): User {
        if (id < 1) throw HandleIllegalStateException("The Id provided is not valid")
        return if (userRepository.existsById(id)) {
            userRepository.save(
                User(
                    name = user.name, email = user.email,
                    password = user.password
                )
            )
        } else throw RecordNotFoundException("No matching employee was found")
    }

    override fun deleteUserById(userId: Long) {
        return if (userRepository.existsById(userId)) {
            userRepository.deleteById(userId)
        } else throw RecordNotFoundException("No matching user was found")
    }

}
