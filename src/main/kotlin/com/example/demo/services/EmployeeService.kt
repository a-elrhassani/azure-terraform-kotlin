package com.example.demo.services

import com.example.demo.models.Employee

interface EmployeeService  {
    fun getEmployeesById(employeeId: Long): Employee
    fun getAllEmployees(): List<Employee>
    fun createEmployee(employee: Employee): Employee
    fun updateEmployeeById(employeeId: Long, employee: Employee): Employee
    fun deleteEmployeesById(employeeId: Long)
}
