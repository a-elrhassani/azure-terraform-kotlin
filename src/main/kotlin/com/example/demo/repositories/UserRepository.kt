package com.example.demo.repositories

import com.example.demo.models.User
import org.springframework.data.jpa.repository.JpaRepository

interface UserRepository: JpaRepository<User, Long> {
    fun findUserByEmail(email:String): User?
}
