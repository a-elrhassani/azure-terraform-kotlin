package com.example.demo.exceptions

class HandleIllegalStateException(message: String) : RuntimeException(message) {
}
