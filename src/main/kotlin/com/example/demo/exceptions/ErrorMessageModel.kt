package com.example.demo.exceptions

data class ErrorMessageModel(
    var status: Int? = null,
    var message: String? = null
)
