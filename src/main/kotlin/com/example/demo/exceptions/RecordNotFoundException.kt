package com.example.demo.exceptions

class RecordNotFoundException(message: String) : RuntimeException(message)
