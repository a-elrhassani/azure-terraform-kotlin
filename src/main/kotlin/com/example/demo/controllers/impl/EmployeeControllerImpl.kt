package com.example.demo.controllers.impl

import com.example.demo.controllers.EmployeeController
import com.example.demo.services.impl.EmployeeServiceImpl
import com.example.demo.models.Employee
import org.springframework.web.bind.annotation.*

@RestController
class Index() {

    @GetMapping("/")
    fun index(): String {
        return "hello dev!"
    }

}

@RestController
@RequestMapping("api")
class EmployeeControllerImpl(private val employeeService: EmployeeServiceImpl) : EmployeeController {

    @GetMapping("/employees")
    override fun getAllEmployees(): List<Employee> = employeeService.getAllEmployees()

    @GetMapping("/employees/{id}")
    override fun getEmployeeById(@PathVariable id: Long): Employee =
        employeeService.getEmployeesById(id)

    @PostMapping("/employees")
    override fun createEmployee(@RequestBody employee: Employee): Employee = employeeService.createEmployee(employee)

    @PutMapping("/employees/{id}")
    override fun updateEmployeeById(@PathVariable id: Long, @RequestBody employee: Employee): Employee =
        employeeService.updateEmployeeById(id, employee)

    @DeleteMapping("/employees/{id}")
    override fun deleteEmployeesById(@PathVariable id: Long) = employeeService.deleteEmployeesById(id)
}
