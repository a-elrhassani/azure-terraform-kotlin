package com.example.demo.controllers.impl

import com.example.demo.controllers.UserController
import com.example.demo.models.User
import com.example.demo.services.impl.UserServiceImpl
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("api")
class UserControllerImpl(private val userService: UserServiceImpl) : UserController {

    @GetMapping("/users/{id}")
    override fun getUserById(@PathVariable id: Long): User = userService.getUserById(id)

    @GetMapping("/users")
    override fun getAllUsers(): List<User>  = userService.getAllUsers()

    @PostMapping("/users")
    override fun createUser(@RequestBody user: User): User = userService.createUser(user)

    @PutMapping("/users/{id}")
    override fun updateUserById(@PathVariable id: Long,@RequestBody user: User): User = userService.updateUser(id, user)

    @DeleteMapping("/users/{id}")
    override fun deleteUsersById(@PathVariable id: Long) {
        TODO("Not yet implemented")
    }
}
