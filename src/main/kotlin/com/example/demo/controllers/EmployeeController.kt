package com.example.demo.controllers

import com.example.demo.models.Employee
import org.springframework.web.bind.annotation.*

interface EmployeeController  {
    @GetMapping("/employees/{id}")
    fun getEmployeeById(@PathVariable id: Long): Employee
    @GetMapping("/employees")
    fun getAllEmployees(): List<Employee>
    @PostMapping("/employees")
    fun createEmployee(@RequestBody employee: Employee): Employee
    @PutMapping("/employees/{id}")
    fun updateEmployeeById(@PathVariable id: Long,@RequestBody employee: Employee): Employee
    @DeleteMapping("/employees/{id}")
    fun deleteEmployeesById(@PathVariable id: Long)
}
