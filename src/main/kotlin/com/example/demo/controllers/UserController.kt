package com.example.demo.controllers

import com.example.demo.models.User
import org.springframework.web.bind.annotation.*

interface UserController {
    @GetMapping("/users/{id}")
    fun getUserById(@PathVariable id: Long): User
    @GetMapping("/users")
    fun getAllUsers(): List<User>
    @PostMapping("/users")
    fun createUser(user: User): User
    @PutMapping("/users/{id}")
    fun updateUserById(@PathVariable id: Long, user: User): User
    @DeleteMapping("/users/{id}")
    fun deleteUsersById(@PathVariable id: Long)

}
