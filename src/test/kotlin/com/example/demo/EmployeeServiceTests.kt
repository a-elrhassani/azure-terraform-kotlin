package com.example.demo

import com.example.demo.models.Employee
import com.example.demo.repositories.EmployeeRepository
import com.example.demo.services.impl.EmployeeServiceImpl
import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.data.repository.findByIdOrNull
import java.util.*


@ExtendWith(MockKExtension::class)
class EmployeeServiceTests {

    private var employeeRepository: EmployeeRepository = mockk(relaxed = true)
    private var employeeServiceImpl = EmployeeServiceImpl(employeeRepository)

    @Test
    fun `should call its data source to retrieve banks`() {
        // where
        employeeServiceImpl.getAllEmployees()
        // then
        verify(exactly = 1) { employeeRepository.findAll() }
    }

    @Test
    fun `should create and then returns employee`() {
        val employee1 = mockk<Employee>()

        every { employeeServiceImpl.createEmployee(employee1) } returns employee1

        employeeServiceImpl.createEmployee(employee1)

        verify { employeeServiceImpl.createEmployee(employee1) }
    }

/*    @Test
    fun whenGetEmployeeAccount_thenReturnEmployee() {
        val employee = mockk<Employee>()

        //given
        every { employeeServiceImpl.getEmployeesById(1) } returns employee

        //when
        val result = employeeRepository.findByIdOrNull(1)

        //then
        verify(exactly = 1) { employeeRepository.findByIdOrNull(1) }
        assertEquals(employee, result)
    }*/

}
